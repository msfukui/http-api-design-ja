## ライセンス

本文書の著作権は [プロジェクトの貢献者達](CONTRIBUTORS.md) に帰属します。

本文書は [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/) の元でリリースします。
